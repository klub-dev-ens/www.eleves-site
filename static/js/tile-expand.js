var current_expanded = null;

function expand_tile(elt) {
    if(current_expanded)
        collapse_tile(current_expanded);
    elt.classList.remove("tile-collapsed");
    elt.classList.add("tile-expanded");

    current_expanded = elt;
}

function collapse_tile(elt) {
    if(elt != current_expanded)
        return;
    current_expanded.classList.remove("tile-expanded");
    current_expanded.classList.add("tile-collapsed");
    current_expanded = null;
}

function on_tile_expand_click(btn_elt) {
    expand_tile(btn_elt.closest(".tile"));
}

function on_tile_collapse_click(btn_elt) {
    collapse_tile(btn_elt.closest(".tile"));
}
