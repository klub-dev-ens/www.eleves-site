---
service_url: https://calc.eleves.ens.fr/
title: Shared calc
shortdescr: Spreadsheets with real-time collaboration.

tags:
- starred
---

# What is it?

Spreadsheets in your browser that multiple people can edit at once.

# What can I use it for?

Do spreadsheets stuff, but also events timetables and registrations, …
