---
service_url: https://www.eleves.ens.fr/pads/
title: Shared pads
shortdescr: Pads with real-time collaboration.

tags:
- starred
---

# What is it?

A basic text document, google docs-like, that multiple people can edit at once.

# What can I use it for?

For meeting reports, to work together on a task while keeping traces, …
