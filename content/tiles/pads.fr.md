---
service_url: https://www.eleves.ens.fr/pads/
title: Pads partagés
shortdescr: Pads collaboratifs en temps réel.

tags:
- starred
---

# C'est quoi ?

Un document texte simple "à la google docs", que vous pouvez éditer à plusieurs
à la fois.

# Ça sert à quoi ?

Aux comptes-rendus de réunion, à bosser ensemble sur un sujet en gardant des
traces, …
