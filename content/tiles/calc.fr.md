---
service_url: https://calc.eleves.ens.fr/
title: Calc partagé
shortdescr: Tableur collaboratif en temps réel.

tags:
- starred
---

# C'est quoi ?

Un tableur dans ton navigateur, que vous pouvez éditer à plusieurs à la fois.

# Ça sert à quoi ?

À faire des trucs de tableurs, mais aussi à faire des plannings d'événements, …
