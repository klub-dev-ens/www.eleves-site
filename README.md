# www.eleves.ens.fr

This project implements the frontpage of
[www.eleves](https://www.eleves.ens.fr).

This is a static website, implemented using the [Hugo](https://gohugo.io/) web
framework.

It is mostly an index of the various IT services offered to the ENS students.

## Install

Begin by [installing Hugo](https://gohugo.io/getting-started/installing/),
probably through your distribution's package manager.

You might want to leaf through the [Hugo
documentation](https://gohugo.io/getting-started/).

### Developper

Run `hugo server` and head to [`localhost:1313`](http://localhost:1313) to
test locally.

### Production

Build the pages with `hugo`.
